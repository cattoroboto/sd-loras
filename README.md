LoRA embeddings for Stable Diffusion

# How to use LoRA with [AUTOMATIC1111 WebUI](https://github.com/AUTOMATIC1111/stable-diffusion-webui)

- Copy LoRA file into models/Lora
- Select LoRA from WebUI in the `Show extra networks`
- It will now be in the prompt like this: `<lora:my_awesome_lora:1>`

>you can change the weight of a lora like with any other prompt `<lora:my_lora:0.7>` or `<lora:my_lora:1.2>`

- Images in the Lora folder with the same name will show up as thumbnails in the UI (like `cat.png` for `cat.safetensors`). This is a great way to store prompts for loras too.

# Available LoRAs

>nsfw loras will only have example prompts in a .txt file

## sinensian artstyle

### parameters

- Epochs: 10
- Steps: 390
- DIM size: 32
- cosine
- no warmup
